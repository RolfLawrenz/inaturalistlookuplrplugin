--[[----------------------------------------------------------------------------

iNaturalistMetadataTagset.lua
Custom metadata tagset

------------------------------------------------------------------------------]]

return {

	title = LOC "$$$/iNaturalistMetadata/Tagset/Title=iNaturalist Metadata",
	id = 'iNaturalistMetadataTagset',

	items = {
		'com.adobe.filename',
		'com.adobe.folder',
		'com.adobe.captureTime',

		'com.adobe.separator',

		'com.adobe.title',
		{ 'com.adobe.caption', height_in_lines = 3 },
		'com.adobe.keywords',
		'com.adobe.location',

		'com.adobe.separator',

		'com.lawrenzcode.lightroom.inaturalist.lookup.*',
	},
}
