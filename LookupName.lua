--[[----------------------------------------------------------------------------
LookupName.lua
Uses iNaturalist API to lookup names.
--------------------------------------------------------------------------------
Rolf Lawrenz
 Copyright 2017 LawrenzCode
 All Rights Reserved.
------------------------------------------------------------------------------]]

-- Access the Lightroom SDK namespaces.
local LrFunctionContext = import 'LrFunctionContext'
local LrBinding = import 'LrBinding'
local LrDialogs = import 'LrDialogs'
local LrView = import 'LrView'
local LrApplication = import 'LrApplication'
local LrStringUtils = import "LrStringUtils"
local LrHttp = import "LrHttp"
local LrMD5 = import "LrMD5"
local LrTasks = import "LrTasks"
local LrDate = import "LrDate"
LrLogger = import 'LrLogger'

JSON = require "JSON.lua"
require "Util.lua"

local DIALOG_CHAR_WIDTH = 30
local RANK_CHAR_WIDTH = 4
local BTN_CHAR_WIDTH = 5
local NAME_CHAR_WIDTH = DIALOG_CHAR_WIDTH - RANK_CHAR_WIDTH - BTN_CHAR_WIDTH
local NAME_BLOCK_COUNT = 4

local SINGLE_DELIM = ","
local MULTI_DELIM = "|"

local inat_data = nil
local selectedIndex = 1
local pluginId = "com.lawrenzcode.lightroom.inaturalist.lookup"

local keyCount = 0

local catalog = LrApplication.activeCatalog()

local function clear_names(pr, index)
  pr['rank'..index] = ''
  pr['common_name'..index] = ''
  pr['sci_name'..index] = ''
end

-- Looks up iNaturalist api and decodes JSON and updates combo box
local function inaturalist_names(keyword, props)
  local url = "https://api.inaturalist.org/v1/taxa/autocomplete?q="..keyword.."&per_page=10"
  local headers = {
    {field='Accept', value='application/json'},
    {field='Content-Type', value='application/json'},
  }
  local items = {}
  if (#keyword > 2) then
    keyCount = keyCount + 1
    -- Call iNaturalist API
    LrTasks.startAsyncTask( function()
      -- Delay for a bit in case another key press. This reduces the number of lookups to API
      local lastKeyCount = keyCount
      LrTasks.sleep(0.6)

      -- If these numbers do not match, another key press has happened so do nothing
      if lastKeyCount ~= keyCount then
        return
      end
      keyCount = 0

      local result, hdrs = LrHttp.get( url, headers )
      inat_data = JSON:decode(result)

      -- Set in labels or name blocks
      local i = 1
      for k,v in pairs(inat_data['results']) do
        items[#items + 1] = v['matched_term']
        if (i <= NAME_BLOCK_COUNT) then
          props['rank'..i] = v['rank']
          props['common_name'..i] = v['preferred_common_name']
          props['sci_name'..i] = v['name']
        end
        i = i + 1
      end
      -- If there are only a few, clear the end name blocks
      if i <= NAME_BLOCK_COUNT then
        for j = i,NAME_BLOCK_COUNT do
          clear_names(props, j)
        end
      end

      -- Set items in combo box
      props.storeItems = items
    end )
  else
    for j = 1,NAME_BLOCK_COUNT do
      clear_names(props, j)
    end
  end
end

-- User pressed the 'Select' button
local function selectBtn(button, index)
  selectedIndex = index
  LrDialogs.stopModalWithResult(button, "ok")
end

-- Group block in dialog with names and select button
local function nameBlock(f, index)
  return f:group_box {
      f:row {
        f:static_text {
          title = LrView.bind ('rank'..index),
          width_in_chars = RANK_CHAR_WIDTH,
          font = "<system/small>"
        },
        f:static_text {
          title = LrView.bind ('common_name'..index),
          width_in_chars = NAME_CHAR_WIDTH,
          font = "<system/bold>"
        },
        f:push_button {
          title = "Select",
          width_in_chars = BTN_CHAR_WIDTH,
          font = "<system>",
          action = function( button )
            selectBtn(button, index)
          end,
        },
      },
      f:row {
        f:static_text {
          title = "",
          width_in_chars = RANK_CHAR_WIDTH,
        },
        f:static_text {
          title = LrView.bind ('sci_name'..index),
          width_in_chars = NAME_CHAR_WIDTH,
          font = "<system>"
        },
      },
    }
end

local function taxonExistsInSelectedPhoto(seldata, photo)
  local taxonIds = photo:getPropertyForPlugin( _PLUGIN, 'iNatTaxonId' )
  local inPhoto = false
  if not isEmpty(taxonIds) then
    local taxonTbl = Split(taxonIds,SINGLE_DELIM,30)
    for i = 1, #taxonTbl do
      if taxonTbl[i] == tostring(seldata['id']) then
        inPhoto = true
        break
      end
    end
  end
  return inPhoto
end

local function taxonExistsInSelectedPhotos(seldata)
  local allPhotos = true
  for index, photo in ipairs(catalog:getTargetPhotos()) do
    local inPhoto = taxonExistsInSelectedPhoto(seldata, photo)
    if inPhoto == false then
      allPhotos = false
      break
    end
  end
  return allPhotos
end

local function appendAndSetProperty(photo, currVal, appendVal, propertyName, delim)
  local str = appendStr(currVal, appendVal, delim)
  photo:setPropertyForPlugin( _PLUGIN, propertyName, str )
end

local function removeAndSetProperty(photo, currVal, index, propertyName, delim)
  local str = removeStrItem(currVal, index, delim)
  photo:setPropertyForPlugin( _PLUGIN, propertyName, str )
end

local function captionName(seldata)
  local cName = seldata['preferred_common_name']
  if cName == nil then
    cName = ""
  end
  if #cName > 0 then
    cName = cName.." "
  end
  if #seldata['name'] > 0 then
    cName = cName.."("..seldata['name']..")"
  end
  return cName
end

-- Tag will have a iNaturalist parent folder
local function keywordTag(seldata)
  local name = seldata['name']
  if name ~= nil then
    name = "iNaturalist|"..name
  end
  return name
end

-- Keyword object
local function getKeyword(keywordPath)
	local catalog = LrApplication.activeCatalog()
	local keywordHierarchy = Split(keywordPath, '|', nil)
	local keyword, parentKeyword = nil, nil

	for i = 1, #keywordHierarchy do
		keyword = catalog:createKeyword(keywordHierarchy[i], {}, true, parentKeyword, true)
		parentKeyword = keyword
	end

	return keyword
end

-- Add keyword to photo if it doesnt exist already
local function addKeywordHierarchyToCatalogAndPhoto(keyword, srcPhoto)
	if srcPhoto and keyword then
	  srcPhoto:addKeyword(keyword)
	end

	return keyword
end

local function removeKeyword(srcPhoto, keywordPath)
	local keywordHierarchy = Split(keywordPath, '|', nil)
  local keywords = srcPhoto:getRawMetadata("keywords")

	for i = 1, #keywordHierarchy do
	  if i == #keywordHierarchy then
      for j = 1, #keywords do
        if keywords[j]:getName() == keywordHierarchy[i] then
          srcPhoto:removeKeyword(keywords[j])
        end
      end
    end
	end
end

-- Check if photo has keyword already
local function hasKeyword(srcPhoto, keyword)
	local keywords = srcPhoto:getRawMetadata("keywords")
  local found = false

	for i = 1, #keywords do
  	if keywords[i]:getName() == keyword then
  		found = true
			break
		end
	end

	return found
end

-- add_remove [1=add,0=remove]
local function updateFields(add_remove, seldata)
  -- add is 1
  catalog:withWriteAccessDo("set iNaturalist Fields", function( context )
    local keyword = getKeyword(keywordTag(seldata))
    for index, photo in ipairs(catalog:getTargetPhotos()) do
      -- Current field values
      local taxonIds = photo:getPropertyForPlugin( _PLUGIN, 'iNatTaxonId' )
      local names = photo:getPropertyForPlugin( _PLUGIN, 'iNatTaxonName' )
      local commonNames = photo:getPropertyForPlugin( _PLUGIN, 'iNatCommonName' )
      local ranks = photo:getPropertyForPlugin( _PLUGIN, 'iNatRank' )
      local sitingGroupIds = photo:getPropertyForPlugin( _PLUGIN, 'iNatSitingGroupId' )
      local caption = photo:getFormattedMetadata('caption')

      local inPhoto = taxonExistsInSelectedPhoto(seldata, photo)
      if add_remove == 1 and inPhoto == false then
        if itemIndex(taxonIds, tostring(seldata['id']), SINGLE_DELIM) < 0 then
          appendAndSetProperty(photo, taxonIds, tostring(seldata['id']), 'iNatTaxonId', SINGLE_DELIM)
          appendAndSetProperty(photo, names, seldata['name'], 'iNatTaxonName', MULTI_DELIM)
          appendAndSetProperty(photo, commonNames, seldata['preferred_common_name'], 'iNatCommonName', MULTI_DELIM)
          appendAndSetProperty(photo, ranks, seldata['rank'], 'iNatRank', SINGLE_DELIM)
          appendAndSetProperty(photo, sitingGroupIds, LrDate.timeToUserFormat( LrDate.currentTime(), "%Y%m%d%H%M%S", false ), 'iNatSitingGroupId', SINGLE_DELIM)
          local newCaption = appendStr(caption, captionName(seldata), MULTI_DELIM)
          photo:setRawMetadata('caption',newCaption)
          if keywordTag(seldata) ~= nil then
            addKeywordHierarchyToCatalogAndPhoto(keyword, photo)
          end
        end
      elseif add_remove == 0 and inPhoto == true then
        local j = itemIndex(taxonIds, tostring(seldata['id']), SINGLE_DELIM)
        if j > 0 then
          removeAndSetProperty(photo, taxonIds, j, 'iNatTaxonId', SINGLE_DELIM)
          removeAndSetProperty(photo, names, j, 'iNatTaxonName', MULTI_DELIM)
          removeAndSetProperty(photo, commonNames, j, 'iNatCommonName', MULTI_DELIM)
          removeAndSetProperty(photo, ranks, j, 'iNatRank', SINGLE_DELIM)
          removeAndSetProperty(photo, sitingGroupIds, j, 'iNatSitingGroupId', SINGLE_DELIM)
          local newCaption = removeStrItem(caption, j, MULTI_DELIM)
          photo:setRawMetadata('caption',newCaption)

          local remKeyword = seldata['name']
          if hasKeyword(photo, remKeyword) then
            removeKeyword(photo, keywordTag(seldata))
          end
        end
      end
    end
  end )
end

local function assignFieldValues()
  local seldata = inat_data['results'][selectedIndex]

  -- Check if selection already exists in all selected photos
  if taxonExistsInSelectedPhotos(seldata) then
    -- If it exists in all selected photos, then remove it from caption and iNaturalist fields
    updateFields(0,seldata)
  else
    -- If it does NOT exist in all selected photos, then add it to caption and iNaturalist fields
    updateFields(1,seldata)
  end
end

local function lookupName()

	LrFunctionContext.callWithContext( "lookupName", function( context )

	    local f = LrView.osFactory()

	    -- Create a bindable table.  Whenever a field in this table changes
	    -- then notifications will be sent.
	    local props = LrBinding.makePropertyTable( context )
	    -- Whenever user presses key in combobox, lookup iNaturalist
      props:addObserver( 'storeValue', function( properties, key, newValue )
        inaturalist_names(newValue,properties)
      end )

	    -- Create the contents for the dialog.
	    local c = f:column {

		    -- Bind the table to the view.  This enables controls to be bound
		    -- to the named field of the 'props' table.
		    bind_to_object = props,

		    f:static_text {
		      fill_horizontal = 1,
		      title = (#catalog:getTargetPhotos()).." Photos Selected"
		    },
		    spacing = f:control_spacing(),
		    f:combo_box {
			    value = LrView.bind 'storeValue',
			    items = LrView.bind 'storeItems',
		      immediate = true,
		      width_in_chars = DIALOG_CHAR_WIDTH,
		    },
	      nameBlock(f, 1),
	      nameBlock(f, 2),
	      nameBlock(f, 3),
	      nameBlock(f, 4),
	    }

	    local result = LrDialogs.presentModalDialog {
			    title = "Lookup iNaturalist Name",
			    contents = c,
	    }
      if result == "ok" then
        LrTasks.startAsyncTask( function()
          assignFieldValues()
        end)
      end

	end) -- end main function

end

-- Now display the dialogs.
lookupName()
