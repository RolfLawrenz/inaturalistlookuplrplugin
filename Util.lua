LrLogger = import 'LrLogger'

-- Logging stuff
myLogger = LrLogger( 'lightroomLogger' )
myLogger:enable( "logfile" ) -- Pass either a string or a table of actions.

local MAX_TAXON_PER_OBSERVATION = 30


function log( message )
  myLogger:trace( message )
end

-- Nice print output of table
function serializeTable(val, name, skipnewlines, depth)
    skipnewlines = skipnewlines or false
    depth = depth or 0

    local tmp = string.rep(" ", depth)

    if name then tmp = tmp .. name .. " = " end

    if type(val) == "table" then
        tmp = tmp .. "{" .. (not skipnewlines and "\n" or "")

        for k, v in pairs(val) do
            tmp =  tmp .. serializeTable(v, k, skipnewlines, depth + 1) .. "," .. (not skipnewlines and "\n" or "")
        end

        tmp = tmp .. string.rep(" ", depth) .. "}"
    elseif type(val) == "number" then
        tmp = tmp .. tostring(val)
    elseif type(val) == "string" then
        tmp = tmp .. string.format("%q", val)
    elseif type(val) == "boolean" then
        tmp = tmp .. (val and "true" or "false")
    else
        tmp = tmp .. "\"[inserializeable datatype:" .. type(val) .. "]\""
    end

    return tmp
end

function Join(items, delim)
  if items == nil or next(items) == nil then
    return ""
  end
  local newstr = ""
  if(#items == 1) then
    return items[1]
  end
  for ii = 1, (#items-1) do
    newstr = newstr .. items[ii] .. delim
  end
  newstr = newstr .. items[#items]
  return newstr
end

function Split(str, delim, maxNb)
   if str == nil or delim == nil then
      return {}
   end

   -- Eliminate bad cases...
   if string.find(str, delim) == nil then
      return { str }
   end
   if maxNb == nil or maxNb < 1 then
      maxNb = 0    -- No limit
   end
   local result = {}
   local pat = "(.-)" .. delim .. "()"
   local nb = 0
   local lastPos
   for part, pos in string.gfind(str, pat) do
      nb = nb + 1
      result[nb] = part
      lastPos = pos
      if nb == maxNb then
         break
      end
   end
   -- Handle the last field
   if nb ~= maxNb then
      result[nb + 1] = string.sub(str, lastPos)
   end
   return result
end

function nonNilStr(str)
  if str == nil then
    return ""
  else
    return str
  end
end

function isEmpty(s)
  return s == nil or s == ''
end

function appendStr(str1, str2, delimiter)
  local s = ""
  if str1 == nil or str1 == "" then
    return nonNilStr(str2)
  end
  return nonNilStr(str1) .. nonNilStr(delimiter) .. nonNilStr(str2)
end

function appendStrUniq(str1, str2, delim)
  -- Check if it already exists in this string. If it does, do not append again
  items = Split(str1, delim, MAX_TAXON_PER_OBSERVATION)
  local found = false
  for i = 1, #items do
    if items[i] == str2 then
      found = true
      break
    end
  end

  if found == true then
    return str1
  else
    return appendStr(str1, str2, delim)
  end
end

function boolToStr(bool)
  if bool == nil then
    return "nil"
  end
  if bool == true then
    return "true"
  elseif bool == false then
    return "false"
  else
    return "not bool"
  end
end

-- Comma delimited string. Return index of item starting at 1
function itemIndex(delimStr,item, delim)
  items = Split(delimStr, delim, MAX_TAXON_PER_OBSERVATION)
  for i = 1, #items do
    if items[i] == item then
      return i
    end
  end
  return -1
end

-- Comma limited string, and remove item at index
function removeStrItem(str, index, delim)
  items = Split(str, delim, MAX_TAXON_PER_OBSERVATION)
  local newItems = {}
  for i = 1, #items do
    if i ~= index then
      newItems[#newItems + 1] = items[i]
    end
  end
  return Join(newItems,delim)
end

