## Introduction #

A Lightroom Plugin for the iNaturalist API which looks up species names and sets them to the selected photos.

## Installing the Plugin
In Lightroom:

* **File | Plug-in Manager**
* Click the **Add** button to add a plugin
* Select the **inaturalist_lookup.lrdevplugin**
* Press **Done**

## Using the Plugin

If you look under the **Library | Plug-in Extras** menu you should see a **iNaturalist** menu with a submenu of **Lookup Name**. 

When you select that menu item a modal dialog will appear. At the top there is a combobox where you can start typing the name of your organism. Once it hits 3 characters it will start querying iNaturalist for matches. If you press enter it will select the top organism.

 Under the combo box there are 4 groups of selections. Each which a **Select** button. If you click the **Select** button it will add or remove the organisms metadata.

If you press the **Ok** button it will select the top organism.

If you press the **Cancel** button, it will not change the metadata fields.

After the dialog closes, it will update the metadata for all the selected photos. To remove the metadata fields, type in the same name of the organism. The plugin will check if the names already exist, and remove them if they do.

If you have some selected photos with an organism name, and some that do not. It will add the organism names for the selected photos that do not have the organism name, and leave the others unaltered.

Metadata fields are defined below.

If you plan to export to iNaturalist, select your photos by a siting group. So if you the same individual specie and then later on saw another individual but of the same species, group them separately.

## Key mapping

In Lightroom you cannot map keys yet, but on a Mac you can through settings.

* Open Settings
* **Keyboard** icon
* **Shortcuts** tab
* **App shortcuts**
* Click **+** button
* Find **Lightroom** in Applications List. If not there, click "Other" and find it
* In Menu Title enter **"Library->Plug-in Extras->   Lookup Name"**. *Note there are 3 spaces before "Lookup Name"*
* Enter your keyboard shortcut

Now you have a keyboard shortcut on a Mac.

## Metadata

You can have multiple organisms per photo. Each metadata field will contain the same number of organisms.

This plugins adds the following metadata fields per photo:

* **Taxon Id**. A comma separated list of Taxon Ids as defined in iNaturalist
* **Taxon Name**. A '|' separated list of Scientific Names as defined in iNaturalist
* **Common Name**. A '|' separated list of Common Names as defined in iNaturalist
* **Rank**. A comma separated list of rank for the organism. For example species, genus, family.
* **Siting Group Id**. A comma separated list of numbers to group an organism by a siting. This is used in iNaturalist to group sitings. The numbers are timestamps. The value is not relevant, but it needs to be unique

Also it will alter the **Caption** field with the Name and Common Name of the organism.

There is a **iNaturalist Metadata** metadata preset you can select to see all these fields at once.

## Coding
Normally when changing code you do not need to reload the plugin. Just try again in Lightroom under:

* Library | Plugin Extras | iNaturalist

But changing some files requires you to remove plugin and add it again. Sometimes restarting Lightroom helps.

## Debugging
When adding new files

* Close Lightroom
* Open **File | Plugin Manager**
* Remove Plugin, and add it again

When running open terminal at ~/Documents

~~~
tail -f -n100 lightroomLogger.log
~~~
