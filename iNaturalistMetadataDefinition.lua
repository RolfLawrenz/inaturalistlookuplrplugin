--[[----------------------------------------------------------------------------

iNaturalistMetadataDefinition.lua
Fields carried over from the iNaturalist website.

------------------------------------------------------------------------------]]

-- require "PluginInit"

return {

	metadataFieldsForPhotos = {
		{
			id = 'iNatTaxonId',
			title = LOC "$$$/iNaturalistMetadata/Fields/iNatTaxonId=Taxon Id",
			dataType = 'string',
			readOnly = false,
			searchable = true,
			browsable = true,
		},
		{
			id = 'iNatTaxonName',
			title = LOC "$$$/iNaturalistMetadata/Fields/iNatTaxonName=Taxon Name",
			dataType = 'string',
			searchable = true,
			browsable = true,
		},
		{
			id = 'iNatCommonName',
			title = LOC "$$$/iNaturalistMetadata/Fields/iNatCommonName=Common Name",
			dataType = 'string',
			searchable = true,
			browsable = true,
		},
		{
			id = 'iNatRank',
			title = LOC "$$$/iNaturalistMetadata/Fields/iNatRank=Rank",
			dataType = 'string',
			searchable = true,
			browsable = true,
			readOnly = true,
		},
        -- Allows us to group the same siting together for publishing
		{
			id = 'iNatSitingGroupId',
			title = LOC "$$$/iNaturalistMetadata/Fields/iNatSitingGroupId=Siting Group Id",
			dataType = 'string',
			readOnly = true,
		},
	},

	schemaVersion = 1, -- must be a number, preferably a positive integer

}
