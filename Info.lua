--[[----------------------------------------------------------------------------

Rolf Lawrenz
 Copyright 2017 LawrenzCode
 All Rights Reserved.

--------------------------------------------------------------------------------

Info.lua
Summary information for iNaturalist Lookup plug-in.

------------------------------------------------------------------------------]]

return {
	
	LrSdkVersion = 3.0,
	LrSdkMinimumVersion = 1.3, -- minimum SDK version required by this plug-in

	LrToolkitIdentifier = 'com.lawrenzcode.lightroom.inaturalist.lookup',

	LrPluginName = LOC "$$$/iNaturalist/PluginName=iNaturalist Lookup",

	LrMetadataProvider = 'iNaturalistMetadataDefinition.lua',

  -- Add the Metadata Tagset File
  LrMetadataTagsetFactory = 'iNaturalistMetadataTagset.lua',

	-- Add the menu item to the Library menu.
	LrLibraryMenuItems = {
	    {
		    title = LOC "$$$/iNaturalist/Lookup=Lookup Name",
		    file = "LookupName.lua",
		},
	},
	VERSION = { major=1, minor=0, revision=1, build=12, },

}
